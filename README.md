# Mappable Media for PHP-FFMPEG

[![Latest Version on Packagist](https://img.shields.io/packagist/v/danjones000/ffmpeg-mappable-media.svg?style=flat-square)](https://packagist.org/packages/danjones000/ffmpeg-mappable-media)
[![Total Downloads](https://img.shields.io/packagist/dt/danjones000/ffmpeg-mappable-media.svg?style=flat-square)](https://packagist.org/packages/danjones000/ffmpeg-mappable-media)

Mappable Media is a plugin for [PHP-FFMpeg](https://packagist.org/packages/php-ffmpeg/php-ffmpeg) which provides a mechanism for mapping individual streams from input to output files.

It allows for greater control regarding specific codecs and metadata from a particular input stream to a particular output stream than the built-in [AdvancedMedia](https://github.com/PHP-FFMpeg/PHP-FFMpeg/#advancedmedia) provides.

## Installation

You can install the package via composer:

```bash
composer require danjones000/ffmpeg-mappable-media
```

## Usage

```php
use Danjones\FFMpeg\MappableMedia;
use FFMpeg\FFMpeg;

$ffmpeg = FFMpeg::create();
$media = MappableMedia::make($ffmpeg);

$media->addInput('video.mp4');
$media->addInput('audio.opus');
$media->addInput('sub.srt');

$output1 = $media->map();
$output1->saveAs('no-subs.webm');
$output1->metadata('title', 'The Greatest Stury Ever Hulaed');

$stream1 = $output1->stream();
$stream1->setInput('0:0');
$stream1->video('libvp9');
$stream1->saveStream();

$stream2 = $output1->stream();
$stream2->setInput('1:0');
$stream2->copy();
$stream2->saveStream();

$output1->saveMap();

$output2 = $media->map();
$output2->saveAs('all-copy.mkv');

// Most methods return the object that calls them.
$output2->stream()->setInput('0:0')->copy()->saveStream();
$output2->stream()->setInput('1:0')->copy()->saveStream();
$output2->stream()->setInput('2:0')->copy()->metadata('language', 'eng')->saveStream();
$output2->saveMap();

$media->save();
```

The `saveStream` method returns the `Map` that created it. Likewise, the `saveMap` returns the `MappableMedia` that created it. All other methods are chainable.

So, this could be written as a very long single call like this:

```php
MappableMedia::make($ffmpeg)
    ->addInput('video.mp4')
    ->addInput('audio.opus')
    ->addInput('sub.srt')
    ->map()
        ->saveAs('no-subs.webm')
        ->metadata('title', 'The Greatest Story Ever Hulaed')
        ->stream()->setInput('0:0')->video('libvp9')->saveStream()
        ->stream()->setInput('1:0')->copy()->saveStream()
        ->saveMap()
    ->map()
        ->saveAs('all-copy.mkv')
        ->stream()->setInput('0:0')->copy()->saveStream()
        ->stream()->setInput('1:0')->copy()->saveStream()
        ->stream()->setInput('2:0')->copy()->metadata('language', 'eng')->saveStream()
        ->saveMap()
    ->save()
 ```

Both of these calls would result in the following command being run:

```shell
ffmpeg -y -i video.mp4 -i audio.opus -i sub.srt -map 0:0 -c:0 libvp9 -map 1:0 -c:1 copy -metadata title="The Greatest Story Ever Hulaed" no-subs.webm -map 0:0 -c:0 copy -map 1:0 -c:1 copy -map 2:0 -c:2 copy -metadata:s:2 language=eng all-copy.mkv
```

This will result in two files. The first will be a webm file (suitable for play in a browser) with a `title` field on the file set to "The Greatest Story Ever Hulaed". The second file will copy the first stream from each file directly, and set the language of the subtitle stream to English.

### Using FormatInterface

It is also possible to set the codec from a stream using the [Formats](https://github.com/PHP-FFMpeg/PHP-FFMpeg/#formats) from PHP-FFMPEG.

```php
use FFMpeg\Format\Video\X264;

MappableMedia::make($ffmpeg)
    ->addInput('input.mkv')
    ->map()
        ->saveAs('output.mkv')
        ->stream()->setCodec(new X264())->saveStream()
        ->saveMap()
    ->save()
```

### Using callbacks

The `map` and `stream` methods can take an optional callback, allowing you to set the properties on those individual objects. When a callback is passed the object itself is returned, allowing you to continue the chain.

```php
MappableMedia::make($ffmpeg)
    ->addInput('input.mkv')
    ->map(function (Map $map) {
        $map->saveAs('output.mkv')
            ->stream(function (Stream $stream) {
                $stream->copy()->setInput('0:0');
            });
    })->save()
```

Note that when using callbacks, you also don't need to call `saveMap()` or `saveStream()`.

### Getting progress updates

It is possible to set a listener on the individual streams, using the Format class. However, this don't reliably report the progress of a particular stream. So, this package adds a listener on the `MappableMedia` object itself, which represents the progress of the entire job.

```php
MappableMedia::make($ffmpeg)
    ->addInput('input.mkv')
    ->map()
        ->saveAs('output.mkv')
        ->stream()->copy()->saveStream()
        ->saveMap()
    ->on('progress', function (MappableMedia $media, int $percent, int $remaining, int $rate) {
        echo "Finished {$percent}% of ", $media->getPathfile(), "\n";
    })
    ->save()
```

### Attachments

Some formats (mkv, for example) support arbitrary data attachments. These can be used as cover art, fonts for subtitles, or any arbitrary data.

FFMpeg does support attachments as an additional input. This works well for images, but can be finicky for other file types. Because of this, FFMpeg also supports an `-attach` flag which can be used to explicitly attach a new stream.

Due to the way FFMpeg handles `-attach` differently than `-i`, these need to be added as streams to a specific map, rather than the whole media. Here are a few examples.

```php
MappableMedia::make($ffmpeg)
    ->addInput('input.mkv')
    ->map()
        ->saveAs('output.mkv')
        ->stream()->copy()->saveStream()
        ->attach('image.jpg')
            ->mime('image/jpeg')
            ->addMetadata('filename', 'cover.jpg')
            ->saveAttachment()
        ->saveMap()
    ->save();
```

In this example, we added cover art to the file. Notice the use of the `mime` method to specify the mime-type. **This must always be done**. Note that we also specified a different filename so that the media player would recognize it as cover art. If you don't specify a filename, the original will be used.

```php
MappableMedia::make($ffmpeg)
    ->addInput('input.mkv')
    ->addInput('subs.ass')
    ->map()
        ->saveAs('output.mkv')
        ->stream()->copy()->saveStream()
        ->stream()->setInput('1:0')->copy()->saveStream()
        ->attach(verdana.ttf')
            ->mime('application/x-truetype-font')
            ->saveAttachment()
        ->saveMap()
    ->save();
```

In this example, we've added a font, which is likely referenced in the subtitle file, `subs.ass`.

## Future Plans

- [ ] Add listeners that return all the stdin/stderr
- [ ] Support itsoffset for inputs
- [ ] Support -t and -ss
  + I need to figure out how this will affect the progress listener.

## Contributing

Please see [CONTRIBUTING](https://codeberg.org/spatie/.github/blob/main/CONTRIBUTING.md) for details.

## Credits

- [Dan Jones](https://codeberg.org/danjones000)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
