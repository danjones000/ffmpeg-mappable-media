<?php

namespace Danjones\FFMpeg;

use FFMpeg\Format\FormatInterface;

class Attachment extends Stream
{
    protected string $input = '';

    public function __construct(Map $map, string $file = '')
    {
        $this->map = $map;
        $this->input = $file;
        // Shouldn't be necessary, but just in case
        $this->codec = new Format\Copy();
    }

    public function mime(string $mime): static
    {
        return $this->addMetadata('mimetype', $mime);
    }

    public function setCodec(FormatInterface $codec): static
    {
        return $this;
    }

    protected function parseCodec(): static
    {
        return $this;
    }

    public function saveAttachment(): Map
    {
        return $this->saveStream();;
    }

    public function buildCommand(int $idx = 0): array
    {
        $commands    = parent::buildCommand($idx);
        $commands[0] = '-attach';

        return $commands;
    }
}
