<?php

namespace Danjones\FFMpeg;

use Alchemy\BinaryDriver\Exception\ExecutionFailureException;
use Evenement\EventEmitterInterface;
use Evenement\EventEmitterTrait;
use FFMpeg\Driver\FFMpegDriver;
use FFMpeg\Exception\RuntimeException;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Format\ProgressListener\AbstractProgressListener;
use FFMpeg\Format\ProgressListener\VideoProgressListener;
use FFMpeg\Media\AbstractMediaType;

/**
 * MappableMedia may have multiple inputs and multiple outputs.
 * This class does not accept filters.
 * But you can set initial and additional parameters of the ffmpeg command.
 *
 * @see http://trac.ffmpeg.org/wiki/Creating%20multiple%20outputs
 */
class MappableMedia extends AbstractMediaType implements EventEmitterInterface
{
    use EventEmitterTrait;

    /**
     * @var string[]
     */
    protected array $inputs = [];

    /** @var Map[] */
    protected array $maps = [];

    /**
     * @var string[]
     */
    protected array $initialParameters = [];

    /**
     * @var string[]
     */
    protected array $additionalParameters = [];

    /** @var AbstractProgressListener[] */
    protected array $progressListeners = [];

    public function __construct(FFMpegDriver $driver, FFProbe $ffprobe)
    {
        // In case of error user will see this text in the error log.
        $pathfile = 'you_can_pass_empty_inputs_array_only_if_you_use_computed_inputs';

        parent::__construct($pathfile, $driver, $ffprobe);
    }

    public static function make(FFMpeg $ffmpeg): static
    {
        return new static($ffmpeg->getFFMpegDriver(), $ffmpeg->getFFProbe());
    }

    public function filters()
    {
        return null;
    }

    public function addInput(string $path): static
    {
        if (empty($this->inputs)) {
            $this->pathfile = $path;
        }

        $this->inputs[] = $path;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getInitialParameters(): array
    {
        return $this->initialParameters;
    }

    /**
     * @param string[] $initialParameters
     */
    public function setInitialParameters(array $initialParameters): static
    {
        $this->initialParameters = $initialParameters;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getAdditionalParameters(): array
    {
        return $this->additionalParameters;
    }

    /**
     * @param string[] $additionalParameters
     */
    public function setAdditionalParameters(array $additionalParameters): static
    {
        $this->additionalParameters = $additionalParameters;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getInputs(): array
    {
        return $this->inputs;
    }

    public function getInputsCount(): int
    {
        return count($this->inputs);
    }

    public function getFinalCommand(): string
    {
        $proc = $this->driver->getProcessBuilderFactory()->create($this->buildCommand());

        return $proc->getCommandLine();
    }

    public function map(callable $callback = null): Map|static
    {
        $map = new Map($this);
        if (!$callback) {
            return $map;
        }

        $callback($map);
        $this->saveMap($map);

        return $this;
    }

    public function saveMap(Map $map): static
    {
        $this->maps[] = $map;
        $this->progressListeners = array_merge($this->progressListeners, $map->getListeners());

        return $this;
    }

    public function save(): void
    {
        $command = $this->buildCommand();
        $this->addListener();

        try {
            $this->driver->command($command, false, $this->progressListeners);
        } catch (ExecutionFailureException $e) {
            throw new RuntimeException('Encoding failed', $e->getCode(), $e);
        }
    }

    protected function addListener(): void
    {
        $self = $this;
        $listener = new VideoProgressListener($this->ffprobe, $this->getPathfile(), 1, 1, 0);

        $listener->on('progress', function (...$args) use ($self) {
            $self->emit('progress', array_merge([$self], $args));
        });

        $this->progressListeners[] = $listener;
    }

    /**
     * @return array
     */
    protected function buildCommand()
    {
        $globalOptions = ['threads', 'filter_threads', 'filter_complex_threads'];

        return array_merge(
            ['-y'],
            $this->buildConfiguredGlobalOptions($globalOptions),
            $this->getInitialParameters(),
            $this->buildInputsPart($this->inputs),
            $this->buildMaps($this->maps),
            $this->getAdditionalParameters()
        );
    }

    /**
     * @param string[] $optionNames
     *
     * @return array
     */
    protected function buildConfiguredGlobalOptions($optionNames)
    {
        $commands = [];
        foreach ($optionNames as $optionName) {
            if (!$this->driver->getConfiguration()->has('ffmpeg.'.$optionName)) {
                continue;
            }

            $commands[] = '-'.$optionName;
            $commands[] = $this->driver->getConfiguration()->get('ffmpeg.'.$optionName);
        }

        return $commands;
    }

    /**
     * Build inputs part of the ffmpeg command.
     *
     * @param string[] $inputs
     */
    protected function buildInputsPart(array $inputs): array
    {
        $commands = [];
        foreach ($inputs as $input) {
            $commands[] = '-i';
            $commands[] = $input;
        }

        return $commands;
    }

    /**
     * @param Map[] $maps
     */
    protected function buildMaps(array $maps): array
    {
        $commands = [];
        foreach($maps as $map) {
            array_push($commands, ...$map->buildCommand());
        }

        return $commands;
    }
}
