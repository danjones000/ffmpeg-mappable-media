<?php

namespace Danjones\FFMpeg\Format;

use FFMpeg\Format\Video\DefaultVideo as BaseVideo;

class DefaultVideo extends BaseVideo
{
    protected bool $supportsBFrames = false;

    public function __construct($codec)
    {
        $this->setVideoCodec($codec);
        $this->kiloBitrate = 0;
        $this->modulus = 0;
    }

    public function setVideoCodec($videoCodec)
    {
        $this->videoCodec = $videoCodec;

        return $this;
    }

    public function getAvailableVideoCodecs()
    {
        return [];
    }

    public function getAvailableAudioCodecs()
    {
        return [];
    }

    public function supportBFrames()
    {
        return $this->supportsBFrames;
    }

    public function setSupportBFrames(bool $supports = true): static
    {
        $this->supportsBFrames = $supports;

        return $this;
    }
}
