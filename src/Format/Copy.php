<?php

namespace Danjones\FFMpeg\Format;

use FFMpeg\Format\FormatInterface;

class Copy implements FormatInterface
{
    public function getPasses()
    {
        return 1;
    }

    public function getExtraParams()
    {
        return [];
    }
}
