<?php

namespace Danjones\FFMpeg\Format;

use FFMpeg\Format\FormatInterface;

interface SubtitleInterface extends FormatInterface
{
    public function getSubtitleCodec();
}
