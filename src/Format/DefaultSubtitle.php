<?php

namespace Danjones\FFMpeg\Format;

class DefaultSubtitle implements SubtitleInterface
{
    public function __construct($codec)
    {
        $this->setSubtitleCodec($codec);
    }

    public function setSubtitleCodec($subtitleCodec)
    {
        $this->subtitleCodec = $subtitleCodec;

        return $this;
    }

    public function getSubtitleCodec()
    {
        return $this->subtitleCodec;
    }

    public function getPasses()
    {
        return 1;
    }

    public function getExtraParams()
    {
        return [];
    }
}
