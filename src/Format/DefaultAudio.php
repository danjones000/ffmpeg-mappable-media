<?php

namespace Danjones\FFMpeg\Format;

use FFMpeg\Format\Audio\DefaultAudio as BaseAudio;

class DefaultAudio extends BaseAudio
{
    public function __construct($codec)
    {
        $this->setAudioCodec($codec);
        $this->audioKiloBitrate = null;
        $this->audioChannels = null;
    }

    public function setAudioCodec($audioCodec)
    {
        $this->audioCodec = $audioCodec;

        return $this;
    }

    public function getAvailableAudioCodecs()
    {
        return [];
    }
}
