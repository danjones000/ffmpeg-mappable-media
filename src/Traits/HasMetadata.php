<?php

namespace Danjones\FFMpeg\Traits;

trait HasMetadata
{
    protected array $metadata = [];

    public function addMetadata(array|string $keyOrData, string $value = null): static
    {
        if (is_array($keyOrData)) {
            array_walk($keyOrData, fn($v, $k) => $this->addMetadata($k, $v));

            return $this;
        }

        $this->metadata[$keyOrData] = $value;

        return $this;
    }
}
