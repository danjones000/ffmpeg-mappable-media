<?php

namespace Danjones\FFMpeg;

use FFMpeg\Format\ProgressableInterface;
use FFMpeg\Format\ProgressListener\AbstractProgressListener;

class Map
{
    use Traits\HasMetadata;

    protected MappableMedia $media;
    protected string $path;
    /** @var Stream[] */
    protected array $streams = [];
    /** @var Attachment[] */
    protected array $attachments = [];
    /** @var AbstractProgressListener[] */
    protected array $listeners = [];

    public function __construct(MappableMedia $media)
    {
        $this->media = $media;
    }

    public function saveMap(): MappableMedia
    {
        $this->media->saveMap($this);

        return $this->media;
    }

    public function saveAs(string $path): static
    {
        $this->path = $path;

        return $this;
    }

    protected function doStream(Stream $stream, callable $callback = null): Stream|static
    {
        if (!$callback) {
            return $stream;
        }

        $callback($stream);
        $this->saveStream($stream);

        return $this;
    }

    public function stream(callable $callback = null): Stream|static
    {
        return $this->doStream(new Stream($this), $callback);
    }

    public function attach(string $file = '', callable $callback = null): Attachment|static
    {
        return $this->doStream(new Attachment($this, $file), $callback);
    }

    public function saveStream(Stream $stream): static
    {
        if ($stream instanceof Attachment){
            $this->attachments[] = $stream;

            return $this;
        }

        $this->streams[] = $stream;
        $format = $stream->getCodec();
        if ($format instanceof ProgressableInterface) {
            $listener = $format->createProgressListener(
                $this->media,
                $this->media->getFFProbe(),
                1, 1, 0
            );
            $this->listeners = array_merge($this->listeners, $listener);
        }

        return $this;
    }

    public function getListeners(): array
    {
        return $this->listeners;
    }

    public function buildCommand(): array
    {
        $commands = [];
        $streams = $this->streams;
        array_push($streams, ...$this->attachments);
        foreach ($streams as $idx => $stream) {
            array_push($commands, ...$stream->buildCommand($idx));
        }
        foreach ($this->metadata as $k => $v) {
            array_push($commands, '-metadata', "{$k}={$v}");
        }
        $commands[] = $this->path;

        return $commands;
    }
}
