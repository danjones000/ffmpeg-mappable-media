# Changelog

All notable changes to `ffmpeg-mappable-media` will be documented in this file.

## 0.2.0

-  ✨ Support attachments

## 0.1.3

- ✨ Support progress listeners
- 📝 Improve documentation

## 0.1.0

🎉 Initial release. Basic functionality works.
